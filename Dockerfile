FROM openjdk:11-jdk-slim

ARG DEPENDENCY=hodarts-backend/build/dependencies
COPY ${DEPENDENCY} /app/lib
ARG LIB=hodarts-backend/build/libs
COPY ${LIB}/ /app/lib

ARG DIST=hodarts-gui/dist
COPY ${DIST} /dist

ENTRYPOINT ["java", "-Dspring.profiles.active=container","-cp","app/lib/*","de.hodarts.backend.HodartsBackendApplicationKt" ]
