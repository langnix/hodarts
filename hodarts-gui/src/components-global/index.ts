/* Components to be registered globally*/
import Vue from 'vue'

import HoButton from './ho-button.vue'
import HoModal from './ho-modal.vue'
import HoSpinner from './ho-spinner.vue'
import HoIcon from './ho-icon.vue'

export default function () {
  Vue.component('ho-button', HoButton)
  Vue.component('ho-modal', HoModal)
  Vue.component('ho-spinner', HoSpinner)
  Vue.component('ho-icon', HoIcon)

}