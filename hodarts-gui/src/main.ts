import Vue from 'vue'
import VueRouter from "vue-router";

import App from './App.vue'

import store from './store';
import routerConfig from './router'

import compInit from '@/components-global'
compInit()

Vue.config.productionTip = false

Vue.use({
  install(Vue, options) {
    Vue.prototype['$store'] = store;
  }
})

const root = new Vue({
  router: routerConfig,
  render: h => h(App),
}).$mount('#app')