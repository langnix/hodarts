
export function getApiUrl() {
  const folder = '/api';
  if (!process.env.VUE_APP_BACKEND) {
    const loc = window.location
    //console.log('location:', loc);
    return `${loc.origin}${folder}`;
  }
  const secure = (process.env.VUE_APP_BACKEND_SECURE) === 'true' ? 's' : '';
  return `http${secure}://${process.env.VUE_APP_BACKEND}${folder}`
}
export function getWsUrl() {
  const folder = '/ws';
  //build a default based on window.location
  if (!process.env.VUE_APP_BACKEND) {
    const loc = window.location
    const secure = loc.protocol == 'https:' ? 's' : '';
    return `ws${secure}://${loc.host}${folder}`;
  }
  const secure = (process.env.VUE_APP_BACKEND_SECURE) === 'true' ? 's' : '';
  return `ws${secure}://${process.env.VUE_APP_BACKEND}${folder}`
}
