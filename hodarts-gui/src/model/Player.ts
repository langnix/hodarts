export interface IPlayer {
  id: string;
  name: string;
  abbrevation: string;
}