import { ITarget } from './Target';
import { ITeam } from './Team';
import { Hit, IHitBackend } from './Hit';
import { IPlayer } from './Player';

export interface IGame {
  id: string;
  teams: ITeam[];
  targets: ITarget[];
  activeTeamNr: number;
  activeRound: number;
  activePlayer: IPlayer;
  complete: Boolean;
  //hits: IHitBackend[];
}
