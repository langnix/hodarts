export interface ITarget {
  id: string;
  label: string;
  noExtra?: boolean;
  noSingleClick?: boolean;
  bullsEye?: boolean;
  value?: number;
}
