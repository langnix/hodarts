import { ITarget } from './Target';
import { Modifier } from './Modifier';

export class Hit {
  target: ITarget;
  modifier: Modifier;
  modifierStr(): string { return Modifier[this.modifier] };
}

export class AccountedHit {
  constructor(public hit : Hit, public usageId: string, public count: number) { }
}

export interface IHitBackend {
  /** die Id des players, der diesen Hit geworfen hat */
  playerId: string;
  /** das Feld, das getroffen wurde */
  targetId: string;
  /** wie das Feld getroffen wurde */
  modifier: string;
  /** das Feld/Wert, auf das der Hit zaehlen soll */
  usageId: string;
}
