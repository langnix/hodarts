import { ITarget } from './Target';

export class Stats {
  constructor(targets: ITarget[]) {
    for (let target of targets) {
      this[target.id] = 0;
    }
  }
}


export class TargetStat {
  targetId: String = '';
  count: number = 0
}

export type statsByTargetId = Map<String, TargetStat>