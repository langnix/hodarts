import { ITarget } from './Target';
import { Stats } from './Stats';
import { AccountedHit } from './Hit';
import { IPlayer } from './Player';

export interface ITeam {
  id: string;
  stats: Stats;
  score: number;
  ticket: number;
  players: IPlayer[]
  activeHits: AccountedHit[];
  //player name
  nameShort: string; 
  complete: boolean;
  winner: boolean;
}
