import { IPlayer } from './Player'
import { IGame } from './Game';

export interface IRoom {
  id: string;
  players: IPlayer[];
  game?: IGame;
  availablePlayers: IPlayer[];
}