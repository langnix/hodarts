import { ITarget } from "./Target";

export function* genTargets(): Generator<ITarget> {
  for (let i = 20; i >= 12; i--) {
    let ident = i;
    yield { id: ident.toString(), label: i.toString(), value: i };
  }
  yield { id: "double", label: "D", noExtra: true, noSingleClick: true };
  yield { id: "triple", label: "T", noExtra: true, noSingleClick: true };
  yield { id: "bull", label: "B", noExtra: true, bullsEye: true, value: 25 };
  yield { id: "miss", label: "-", noExtra: true, value: 0 };
}
