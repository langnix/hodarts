import Vue from 'vue'
import VueRouter from "vue-router";

import Home from '@/views/Home.vue';
import Profile from '@/views/Profile.vue';
import Lobby from '@/views/Lobby.vue';
import Room from '@/views/Room.vue';
import RoomSetup from '@/views/RoomSetup.vue';
import Cricket from "@/views/Cricket.vue";
import GameSetup from "@/views/GameSetup.vue";

// Routes under Room
const roomRoutes = [
  { path: 'setup', name: 'roomSetup', component: RoomSetup, props: true },
  { path: 'teams', name: 'gameSetup', component: GameSetup, props: true },
  { path: 'cricket', name: 'cricket', component: Cricket, props: true }
];

//Top Level Routes
const routes = [
  { path: '*', redirect: '/home' },
  { path: '/home', name: 'home', component: Home },
  { path: '/profile', name:'profile', component: Profile },
  { path: '/lobby', name: 'lobby', component: Lobby },
  { path: '/room/:roomId', name: 'room', component: Room, props: true , children: roomRoutes},
]

// Create the router instance and pass the `routes` option
Vue.use(VueRouter);
export default new VueRouter({
  mode: 'hash',
  routes 
});
