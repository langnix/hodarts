import Vue from 'vue'
import { Store } from '@/store'

declare module '*.vue' {
  export default Vue
}

declare module "vue/types/vue" {
  interface Vue {
    $store: Store;
  }
}