
import { IPlayer, IRoom } from '@/model'

export class State {
  //
  players: IPlayer[] = [];
  //
  rooms: IRoom[] = [];
  room: IRoom | null = null;
  readyState: number = -1;
} 