import { IRoom, IHitBackend } from '@/model';

const keepAliveIntervalSecs = 60;
export class RoomSock {
  fullUrl: string = '';
  ws: WebSocket | null = null;
  state: number = WebSocket.CONNECTING;
  room: IRoom | null = null;
  roomId: string | null = null;
  kaTimer: number = 0;
  /**
   * 
   * @param baseUrl full Url starting with ws
   * @param messageCb Callback func ro receive Room
   */
  constructor(public baseUrl: string, protected messageCb: (msg: IRoom | null) => void, protected stateCb: (state: number) => void) {
    console.assert(messageCb && true, "message callback is missing");
    console.assert(stateCb && true, "state callback is missing");
    console.assert((baseUrl || '') != '', "baseUrl missing");
  }

  /**
   * returns one of
    0 = WebSocket.CONNECTING;
    1 = WebSocket.OPEN;
    2 = WebSocket.CLOSING;
    3 = WebSocket.CLOSED;   
  */
  getState() {
    return this.state;
  }

  async connectToWebsocket(roomId: string) {
    console.debug("init", roomId);

    if (roomId == this.roomId && this.state == WebSocket.OPEN) {
      console.debug("re-init,already open", roomId);
      return
    }
    // close the old one ...
    this.close();

    this.fullUrl = this.baseUrl + "/room/" + roomId;
    console.debug("getWsurl", this.fullUrl);

    // create a new one
    this.roomId = roomId;
    this.ws = new WebSocket(this.fullUrl)

    // expose this down...
    const self = this;

    this.ws.onopen = function (ev: Event) {
      console.debug('connection established');
      self.state = this.readyState;
      self.stateCb(self.state);
    };
    this.ws.onclose = function (ev: CloseEvent) {
      console.debug('connection closed, code:', ev.code)
      self.state = this.readyState;
      self.stateCb(self.state);
    };
    this.ws.onerror = function (ev: Event) {
      console.debug('error: ', ev)
      self.state = this.readyState;
      self.stateCb(self.state);
    };
    this.ws.onmessage = function (ev: MessageEvent) {
      const room = JSON.parse(ev.data) as IRoom;
      console.debug('onMessage: ', room)
      self.state = this.readyState;
      self.stateCb(self.state);

      self.room = room;
      // no call back after call to close()
      if (self.ws)
        self.messageCb(room);
    };

    //Keepalive
    const id = Math.floor( Math.random() * Math.pow(2,31));
    this.kaTimer = setInterval(() => this.keepAlive(id), keepAliveIntervalSecs * 1000);
  }
  close() {
    console.debug("close", this.roomId);
    if (this.kaTimer) {
      const oldTimer = this.kaTimer
      this.kaTimer = 0;
      clearInterval(oldTimer);
    }
    if (this.ws) {
      const ws = this.ws;
      this.ws = null;
      this.roomId = null;
      ws.close();
      this.messageCb(null)
    }
  }
  keepAlive(n: number) {
    const req = {
      cmd: 'keepAlive', param: { dummy: n }
    }
    if (this.kaTimer && (this.ws?.readyState == WebSocket.OPEN))
      this.ws?.send(JSON.stringify(req))
  }
  joinRoom(playerId: string) {
    const req = { cmd: 'room', param: { playerId: playerId, operation: 'join' } }
    this.ws?.send(JSON.stringify(req))
  }

  leaveRoom(playerId: string) {
    const req = { cmd: 'room', param: { playerId: playerId, operation: 'leave' } }
    this.ws?.send(JSON.stringify(req))
  }

  newGame(nrOfTeams: number) {
    const req = { cmd: 'gameCreate', param: { nrOfTeams: nrOfTeams } }
    this.ws?.send(JSON.stringify(req))
  }
  joinTeam(teamId: string, playerId: string) {
    const req = {
      cmd: 'team',
      param: { teamId: teamId, playerId: playerId, operation: 'join' }
    }
    this.ws?.send(JSON.stringify(req))
  }
  leaveTeam(teamId: string, playerId: string) {
    const req = {
      cmd: 'team',
      param: { teamId: teamId, playerId: playerId, operation: 'leave' }
    }
    this.ws?.send(JSON.stringify(req))
  }
  sendHit(hit: IHitBackend) {
    const req = {
      cmd: 'hit',
      param: { playerId: hit.playerId, targetId: hit.targetId, modifier: hit.modifier, usageId: hit.usageId }
    }
    this.ws?.send(JSON.stringify(req))
  }
  finishPlay(playerId: string) {
    const req = {
      cmd: 'finishPlay',
      param: { playerId }
    }
    this.ws?.send(JSON.stringify(req))
  }
  sendUndo(count: number) {
    const req = {
      cmd: 'undo',
      param: { count }
    }
    this.ws?.send(JSON.stringify(req))
  }
}
