import { Vue, Component, Prop } from 'vue-property-decorator'

import axios from "axios"

import { State } from './state'
import { RoomSock } from './RoomSock'

import { IPlayer, IRoom } from '@/model'
import { getApiUrl, getWsUrl } from '@/settings'

const version = require('../../package.json').version

@Component
export class Store extends Vue {
  @Prop([String]) readonly apiUrl: string 
  @Prop([String]) readonly wsUrl: string 

  appVer: string = version;
  state = new State();
  roomCmd: RoomSock | null = null;

  //never use ctor in vue @component as class def, use created() instead
  created() {
    console.log('Store ctor REST', this.apiUrl);
    console.log('Store ctor WS  ', this.wsUrl);
    this.roomCmd = new RoomSock(this.wsUrl, (r) => this.state.room = r, (s) => this.state.readyState = s)
  }

  // beforeDestroy() {
  //   console.log('beforeDestroy Store');
  //   this.roomCmd?.close();
  // }

  async getRooms() {
    console.debug("fetching rooms", this.apiUrl)
    const response = await axios.get(this.apiUrl + "/rooms")
    this.state.rooms = response.data as IRoom[]
  }

  async getPlayers() {
    console.debug("fetching players", this.apiUrl)
    const response = await axios.get(this.apiUrl + "/players")
    this.state.players = response.data as IPlayer[]
  }

  connectToWebsocket(roomId :string) {
    return this.roomCmd?.connectToWebsocket(roomId);
  }
  disconnectFromWebsocket() {
    return this.roomCmd?.close();
  }
};

console.debug('Api Url:', getApiUrl())
console.debug('Ws Url:', getWsUrl())
console.debug('Env:', process.env)
export default new Store({ propsData: { apiUrl: getApiUrl(), wsUrl: getWsUrl() } })