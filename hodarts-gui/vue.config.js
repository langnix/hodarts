// vue.config.js
module.exports = {
  // options...
  devServer: {
    disableHostCheck: true,
  },
  parallel: true,
  //When deploying into a subfolder, it must be set here - value will also be used by the vue router 
  publicPath: '/', // ve cli 3.3 and above
}