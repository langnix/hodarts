# hodarts-gui

## Configuration
Backend url configured in .env file
You may create a .env.local file with personal test settings. This takes precedence over .env

examples
```
VUE_APP_BACKEND=lala.fritz.box:8088
VUE_APP_BACKEND_SECURE=false
```
or
```
VUE_APP_BACKEND=hodarts.somewhere.de
VUE_APP_BACKEND_SECURE=true
```
The Project expects to be installed in a root dir. You can change this by setting the subfolder
publicPath in vue.config.js

A rebuild is required after those changes


## Project setup
before build for deployment run
```
npm ci
```
This installs precisely the module versions recorded in package-lock.json


### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
