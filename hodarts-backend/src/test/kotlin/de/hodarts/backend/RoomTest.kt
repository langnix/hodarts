package de.hodarts.backend

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class RoomTest {

  @Test
  internal fun `available players contains only unassigned players`() {
    val gl = GameLogic()
    val cut = Room(id = "1")
    assertThat(cut.availablePlayers()).isEmpty()

    val p1 = Player("1", "1", "One")
    cut.add(p1)
    assertThat(cut.availablePlayers()).contains(p1)
    val game = gl.newGame(1)
    cut.game = game
    assertThat(cut.availablePlayers()).contains(p1)
    game.teams[0].addPlayer(p1)
    assertThat(cut.availablePlayers()).isEmpty()
  }

  @Test
  internal fun `available players are rendered`() {
    val objectMapper = jacksonObjectMapper()
    val cut = Room(id = "1")
    val p1 = Player("1", "1", "One")
    cut.add(p1)
    val roomStr = objectMapper.writeValueAsString(cut)

    val roomTree = objectMapper.readTree(roomStr)
    assertThat(roomTree.has("players")).isTrue()
    assertThat(roomTree.has("availablePlayers")).isTrue()
  }

}