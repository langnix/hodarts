package de.hodarts.backend

import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test

internal class HitTest {
    @Test
    internal fun `score of numbers and bull`() {
        Assertions.assertThat(Hit("1", "19", ModifierEnum.single).score()).isEqualTo(19)
        Assertions.assertThat(Hit("1", "12", ModifierEnum.single).score()).isEqualTo(12)
        Assertions.assertThat(Hit("1", "bull", ModifierEnum.single).score()).isEqualTo(25)
        Assertions.assertThat(Hit("1", "miss", ModifierEnum.single).score()).isEqualTo(0)
    }

    @Test
    internal fun count() {
        Assertions.assertThat(Hit("1", "19", ModifierEnum.single).count()).isEqualTo(1)
        Assertions.assertThat(Hit("1", "19", ModifierEnum.triple).count()).isEqualTo(3)
        Assertions.assertThat(Hit("1", "double", ModifierEnum.triple).count()).isEqualTo(1)
    }

    @Test
    internal fun `20 double with usage double scores 40`() {
        Assertions.assertThat(Hit("1", "20", ModifierEnum.double, "double").score()).isEqualTo(40)
    }

    @Test
    internal fun `20 double with usage 20 scores 20`() {
        Assertions.assertThat(Hit("1", "20", ModifierEnum.double, "20").score()).isEqualTo(20)
    }
}