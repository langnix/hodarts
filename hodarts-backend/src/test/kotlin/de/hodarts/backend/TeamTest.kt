package de.hodarts.backend

import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import de.hodarts.backend.ModifierEnum.*
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test


internal class TeamTest {
  val game = GameLogic().newGame(1)

  @Test
  fun useHit() {

    val cut = Team(game, "teamId", mapOf("20" to TargetStat("20"),
            "19" to TargetStat("19"), "miss" to TargetStat("miss"))
    )

    assertThat(cut.countOf("miss")).isEqualTo(0)


    assertThat(cut.useHitOnTarget(Hit("1", "miss", single))).isEqualTo(0)
    assertThat(cut.countOf("miss")).isEqualTo(1)

    assertThat(cut.useHitOnTarget(Hit("1", "miss", double))).isEqualTo(0)
    assertThat(cut.countOf("miss")).isEqualTo(3)

    assertThat(cut.useHitOnTarget(Hit("1", "miss", triple))).isEqualTo(0)
    assertThat(cut.countOf("miss")).isEqualTo(6)


    assertThat(cut.useHitOnTarget(Hit("1", "20", single))).isEqualTo(0)
    assertThat(cut.useHitOnTarget(Hit("1", "20", double))).isEqualTo(0)
    assertThat(cut.useHitOnTarget(Hit("1", "20", triple))).isEqualTo(3)
    assertThat(cut.useHitOnTarget(Hit("1", "20", single))).isEqualTo(1)

    assertThat(cut.useHitOnTarget(Hit("1", "19", single))).isEqualTo(0)
    assertThat(cut.useHitOnTarget(Hit("1", "19", triple))).isEqualTo(1)

  }

  @Test
  internal fun acceptOfMissIsIgnored() {
    val cut = Team(game, "teamId", mapOf("miss" to TargetStat("miss")), 99)
    assertThat(cut.score).isEqualTo(99)
    assertThat(cut.acceptHit(Hit("1", "miss", single), 99)).isEqualTo(99)
  }

  @Test
  internal fun `accept on completed target is ignored`() {
    val cut = Team(game, "teamId", mapOf("19" to TargetStat("19", 3)), 99)
    assertThat(cut.score).isEqualTo(99)
    assertThat(cut.acceptHit(Hit("1", "19", single), 99)).isEqualTo(99)
  }

  @Test
  internal fun `accept on open target is added to score`() {
    val cut = Team(game, "teamId", mapOf("19" to TargetStat("19", 0)), 100)
    assertThat(cut.score).isEqualTo(100)
    assertThat(cut.acceptHit(Hit("1", "19", triple), 2)).isEqualTo(138)
  }

  @Test
  internal fun `active player in team on roundNr`() {
    val teamWithOnePlayer = Team(game, "teamId", emptyMap(), 0)
    teamWithOnePlayer.addPlayer(Player("1", "one", "A"))
    assertThat(teamWithOnePlayer.activePlayerInRound(0)?.name).isEqualTo("one")
    assertThat(teamWithOnePlayer.activePlayerInRound(99)?.name).isEqualTo("one")


    val teamWithTwoPlayer = teamWithOnePlayer.addPlayer(Player("2", "two", "B"))
    assertThat(teamWithTwoPlayer.activePlayerInRound(0)?.name).isEqualTo("one")
    assertThat(teamWithTwoPlayer.activePlayerInRound(1)?.name).isEqualTo("two")

  }

  @Test
  internal fun `team is complete, when all stats are filled`() {
    val allTargets = GameLogic().genTargets()
    val cut = GameLogic().genTeam(game, 1, allTargets)

    assertThat(cut.isComplete()).isFalse()

    cut.statsByTargetId.forEach { _, s -> s.count = 3 }
    assertThat(cut.isComplete()).isTrue()

    cut.statsByTargetId["13"]?.let { it -> it.count-- }
    assertThat(cut.isComplete()).isFalse()
    cut.statsByTargetId["13"]?.let { it -> it.count++ }
    cut.statsByTargetId["miss"]?.let { it -> it.count = 0 }
    assertThat(cut.isComplete()).isTrue()

    val objectMapper = jacksonObjectMapper()
    val teamTree = objectMapper.valueToTree<ObjectNode>(cut)
    assertThat(teamTree.get("complete").asBoolean()).isTrue()
  }

  @Test
  internal fun `the all teams with the min score are winners`() {
    var game3Teams = GameLogic().newGame(3)
    game3Teams.teams.forEach { t -> t.statsByTargetId.forEach { _, s -> s.count = 3 } }
    assertThat(game3Teams.isComplete()).isTrue()
    game3Teams.teams[0].score = 10
    game3Teams.teams[1].score = 20
    game3Teams.teams[2].score = 10

    assertThat(game3Teams.teams[0].isWinner()).isTrue()
    assertThat(game3Teams.teams[1].isWinner()).isFalse()
    assertThat(game3Teams.teams[2].isWinner()).isTrue()
  }
}

