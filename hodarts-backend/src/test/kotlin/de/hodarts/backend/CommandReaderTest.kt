package de.hodarts.backend

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatCode
import org.junit.jupiter.api.Test

internal class CommandReaderTest {
  val mapper = jacksonObjectMapper()

  @Test
  internal fun `write and read Commands`() {
    val cmdBase = Command(CommandEnum.room, RoomParam(PlayerGroupOperation.join, "0815"))
    val cmdStr = mapper.writeValueAsString(cmdBase)
    val cut = CommandReader(mapper)

    val cmd = cut.fromInputStream(cmdStr.byteInputStream())
    assertThat(cmd.cmd).isEqualByComparingTo(CommandEnum.room)
    assertThat((cmd.param as RoomParam).playerId).isEqualTo("0815")


  }

  @Test
  fun vanillaJoin() {
    val roomReqStr = """
      {"playerId":"p123","operation":"join"}
    """.trimIndent()
    val roomReq = mapper.readValue<RoomParam>(roomReqStr.byteInputStream(), RoomParam::class.java)
    assertThat(roomReq.operation).isEqualByComparingTo(PlayerGroupOperation.join)
    assertThat(roomReq.playerId).isEqualTo("p123")

    val joinCmdStr = """
      { "cmd":"room", "param": $roomReqStr }
    """.trimIndent()

    val cut = CommandReader(mapper)

    val cmd = cut.fromInputStream(joinCmdStr.byteInputStream())
    assertThat(cmd.cmd).isEqualByComparingTo(CommandEnum.room)
    assertThat(cmd.param).isEqualTo(roomReq)


  }

  @Test
  internal fun vanillaHit() {
    val hit = HitParam(playerId = "1", targetId = "XX", modifier = ModifierEnum.triple, usageId = "XX")
    val hitStr = mapper.writeValueAsString(hit)
    val cmdStr = """
      { "cmd":"hit", "param": $hitStr }
    """.trimIndent()

    val cut = CommandReader(mapper)

    val cmd = cut.fromInputStream(cmdStr.byteInputStream())
    assertThat(cmd.cmd).isEqualByComparingTo(CommandEnum.hit)
    assertThat(cmd.param).isEqualTo(hit)


  }

  @Test
  internal fun failOnCrunch() {
    val cut = CommandReader(mapper)
    assertThatCode { cut.fromInputStream("".byteInputStream()) }.isInstanceOf(IllegalArgumentException::class.java)
    assertThatCode { cut.fromInputStream("{}".byteInputStream()) }.isInstanceOf(IllegalArgumentException::class.java)
    assertThatCode { cut.fromInputStream("{ \"cmd\": \"unknown\" }".byteInputStream()) }.isInstanceOf(IllegalArgumentException::class.java)
  }
}