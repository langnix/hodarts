package de.hodarts.backend

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import mu.KotlinLogging
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

private val log = KotlinLogging.logger {}

internal class GameTest {
  @Test
  internal fun vanilla() {
    val cut = GameLogic().newGame(3)
    cut.activeTeam().addPlayer(Player("1", "one", "A"))
    assertThat(cut.activeTeamNr()).isEqualTo(0)

    cut.addHit(Hit("1", "20", ModifierEnum.triple, "20"))
    assertThat(cut.activeTeamNr()).isEqualTo(0)
    for (t in cut.teams) assertThat(t.score).isEqualTo(0)

    cut.addHit(Hit("1", "20", ModifierEnum.triple, "20"))
    assertThat(cut.activeTeamNr()).isEqualTo(0)
    assertThat(cut.activeTeam().score).isEqualTo(0)
    for (t in cut.teams.filterNot { t -> t == cut.activeTeam() }) assertThat(t.score).isEqualTo(60)

    cut.addHit(Hit("1", "miss", ModifierEnum.single))
    assertThat(cut.activeTeamNr()).isEqualTo(1)
  }


  @Test
  internal fun `activeHits contains correct hits`() {
    val game = genThreePlayerGame()
    val team0 = game.teams[0]
    val team1 = game.teams[1]
    val team2 = game.teams[2]

    assertThat(team0.activeHits()).isEmpty()
    assertThat(team1.activeHits()).isEmpty()
    assertThat(team2.activeHits()).isEmpty()
    // play 0
    val h1 = Hit("1", "12", ModifierEnum.single)
    game.addHit(h1)
    assertThat(game.activeHitsOfTeam(team0)).hasSize(1)
    assertThat(game.activeHitsOfTeam(team0)[0]).isEqualTo(h1)
    assertThat(team1.activeHits()).isEmpty()
    assertThat(team2.activeHits()).isEmpty()


    val h2 = Hit("1", "12", ModifierEnum.single)
    game.addHit(h2)
    assertThat(game.activeHitsOfTeam(team0)).hasSize(2)
    assertThat(game.activeHitsOfTeam(team0)[0]).isEqualTo(h1)
    assertThat(game.activeHitsOfTeam(team0)[1]).isEqualTo(h2)
    assertThat(team1.activeHits()).isEmpty()
    assertThat(team2.activeHits()).isEmpty()


    val h3 = Hit("1", "12", ModifierEnum.single)
    game.addHit(h3)
    assertThat(game.activeHitsOfTeam(team0)).hasSize(3)
    assertThat(game.activeHitsOfTeam(team0)[0]).isEqualTo(h1)
    assertThat(game.activeHitsOfTeam(team0)[1]).isEqualTo(h2)
    assertThat(game.activeHitsOfTeam(team0)[2]).isEqualTo(h3)
    // play 1
    val p1 = Hit("2", "13", ModifierEnum.single)
    game.addHit(p1)
    assertThat(game.activeHitsOfTeam(team0)).hasSize(3)
    assertThat(team2.activeHits()).isEmpty()
    game.addHit(p1)
    assertThat(game.activeHitsOfTeam(team0)).hasSize(3)
    game.addHit(p1)

    // play 2
    assertThat(game.activeHitsOfTeam(team0)).hasSize(3)
    assertThat(game.activeHitsOfTeam(team1)).hasSize(3)
    assertThat(game.activeHitsOfTeam(team1)[0]).isEqualTo(p1)
    assertThat(team2.activeHits()).isEmpty()

    val p2 = Hit("3", "14", ModifierEnum.single)
    game.addHit(p2); game.addHit(p2); game.addHit(p2)

    assertThat(game.activeHitsOfTeam(team0)).withFailMessage("fresh round on team 1").hasSize(0)
  }

  @Test
  internal fun `double hit as number scrores double on other teams`() {
    val game = genThreePlayerGame()


    val d20 = Hit("1", "20", ModifierEnum.double, "20")
    val m1 = Hit("1", "miss", ModifierEnum.single, "miss")
    game.addHit(d20)
    game.addHit(d20)
    game.addHit(m1)
    assertThat(game.teams[0].isTargetComplete("20")).isEqualTo(true)
    assertThat(game.teams[1].score).isEqualTo(20)
    assertThat(game.teams[2].score).isEqualTo(20)

    val m2 = Hit("2", "miss", ModifierEnum.single, "miss")
    for (i in 1..3) game.addHit(m2)
    val m3 = Hit("3", "miss", ModifierEnum.single, "miss")
    for (i in 1..3) game.addHit(m3)

    game.addHit(d20)
    assertThat(game.teams[1].score).isEqualTo(60)
    assertThat(game.teams[2].score).isEqualTo(60)
  }

  @Test
  internal fun `finish Play counts as remaining`() {
    val game = genThreePlayerGame()
    game.finishPlay(FinishParam("1"))
    assertThat(game.teams[0].stats().getValue("miss")).isEqualTo(3)
    assertThat(game.hits.size).isEqualTo(3)
    assertThat(game.teams[0].ticket).isEqualTo(1)

    val p2 = Hit("2", "20", ModifierEnum.single, "20")
    game.addHit(p2)
    game.finishPlay(FinishParam("2"))
    assertThat(game.hits.size).isEqualTo(6)
    assertThat(game.teams[1].stats().getValue("miss")).isEqualTo(2)
    assertThat(game.teams[1].ticket).isEqualTo(0)

    val p3 = Hit("3", "20", ModifierEnum.single, "20")
    game.addHit(p3)
    game.addHit(p3)
    game.finishPlay(FinishParam("3"))
    assertThat(game.hits.size).isEqualTo(9)
    assertThat(game.teams[2].stats().getValue("miss")).isEqualTo(1)
  }

  @Test
  internal fun `double hit as double scrores double on other teams`() {
    val game = genThreePlayerGame()


    val d20 = Hit("1", "20", ModifierEnum.double, "double")

    game.addHit(d20)
    game.addHit(d20)
    game.addHit(d20)
    assertThat(game.teams[0].isTargetComplete("double")).isEqualTo(true)
    assertThat(game.teams[1].score).isEqualTo(0)
    assertThat(game.teams[2].score).isEqualTo(0)


    val m2 = Hit("2", "miss", ModifierEnum.single, "miss")
    for (i in 1..3) game.addHit(m2)
    val m3 = Hit("3", "miss", ModifierEnum.single, "miss")
    for (i in 1..3) game.addHit(m3)


    game.addHit(d20)
    assertThat(game.teams[1].score).isEqualTo(40)
    assertThat(game.teams[2].score).isEqualTo(40)
  }

  private fun genThreePlayerGame(): Game {
    val game = GameLogic().newGame(3)
    game.teams[0].addPlayer(Player("1", "one", "A"))
    game.teams[1].addPlayer(Player("2", "two", "B"))
    game.teams[2].addPlayer(Player("3", "three", "C"))
    return game
  }

  @Test
  internal fun `the active player is`() {
    val game = genThreePlayerGame()
    game.teams[0].addPlayer(Player("11", "one.1", "A1"))
    game.teams[1].addPlayer(Player("21", "two.1", "B1"))
    game.teams[2].addPlayer(Player("3", "three", "C"))
    assertThat(game.activePlayer()?.name).isEqualTo("one")

    game.addHit(genMiss("1"))
    assertThat(game.activePlayer()?.id).isEqualTo("1")
    game.addHit(genMiss("1"));game.addHit(genMiss("1"))
    assertThat(game.activePlayer()?.id).isEqualTo("2")
    for (i in 1..3) game.addHit(genMiss("2"))
    assertThat(game.activePlayer()?.id).isEqualTo("3")
    for (i in 1..3) game.addHit(genMiss("3"))
    assertThat(game.activePlayer()?.id).isEqualTo("11")

  }

  private fun genMiss(playerId: String): Hit = Hit(playerId, "miss", ModifierEnum.single, "miss")

  @Test
  internal fun `game can be rendered at any time`() {
    val objectMapper = jacksonObjectMapper()
    val game = GameLogic().newGame(3)
    assertThat(game.activeTeam().players).isEmpty()
    assertThat(game.activePlayer()).isNull()
    assertThat(game.activeRound()).isEqualTo(0)

    objectMapper.writeValueAsString(game)
  }


}