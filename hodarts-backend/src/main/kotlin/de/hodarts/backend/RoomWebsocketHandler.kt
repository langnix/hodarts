package de.hodarts.backend

import com.fasterxml.jackson.databind.ObjectMapper
import mu.KotlinLogging
import org.springframework.stereotype.Component
import org.springframework.web.reactive.socket.WebSocketHandler
import org.springframework.web.reactive.socket.WebSocketSession
import org.springframework.web.util.UriComponentsBuilder
import reactor.core.publisher.DirectProcessor
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.net.URI

private val log = KotlinLogging.logger {}


@Component
class RoomWebsocketHandler(val mapper: ObjectMapper, val commandReader: CommandReader, val gameBuilder: GameLogic) : WebSocketHandler {

  // pro Room ein DirectProcssor...
  val processorOfRoom = mutableMapOf<String, DirectProcessor<Room>>()

  override fun handle(session: WebSocketSession): Mono<Void> {


    val uriComp = UriComponentsBuilder.fromUri(session.handshakeInfo.uri).build()
    log.trace { "connecting to:${uriComp.path}" }
    val roomId = uriComp.pathSegments.last()
    val roomToUse = findRoom(roomId)

    log.info { "start subscription:$session on room: $roomId" }
    // Obacht! die map wieder aufraeumen..
    val directProcessor = processorOfRoom.computeIfAbsent(roomId,
            { id ->
              val res = DirectProcessor.create<Room>()
              log.info { "start room: $id with: ${res.hashCode()}" }
              res
            })
    // ... einer hat sich abgemeldet ..
    val removeProcessor = Runnable { ->
      log.info { "finished subscription:$session on room: $roomId , still: ${directProcessor.downstreamCount()}" }
      if (!directProcessor.hasDownstreams()) {
        log.info { "end room: $roomId with: ${directProcessor.hashCode()}" }
        processorOfRoom.remove(roomId)
      }
    }

    val answerFlux = Flux.concat(
            Flux.just(roomToUse), // initial ...
            directProcessor) // triggerd by request
            // TODO: statt textMessage -> byte-buffer schreiben
            .map { room ->
              session.textMessage(mapper.writeValueAsString(room))
            }


    val recFlux = session.receive()
            .map { ev -> commandReader.fromInputStream(ev.payload.asInputStream()) }
            // logic inline ...
            .map { jj ->
              when (jj.cmd) {
                CommandEnum.room -> handleRoomCmd(roomToUse, jj.param as RoomParam)
                CommandEnum.gameCreate -> handleGameCreate(roomToUse, jj.param as GameCreateParam)
                CommandEnum.team -> handleTeamCmd(roomToUse, jj.param as TeamParam)
                CommandEnum.hit -> handleHitCmd(roomToUse, jj.param as HitParam)
                CommandEnum.finishPlay -> handleFinishPlayCmd(roomToUse, jj.param as FinishParam)
                CommandEnum.undo -> handleUndoCmd(roomToUse, jj.param as UndoParam)
                CommandEnum.keepAlive -> handleKeepalive(jj.param as KeepAliveParam)

              }
            }
            //.log()
            // dispatch
            .doOnNext { _ -> directProcessor.onNext(roomToUse) }
            .doOnError { t: Throwable -> log.warn("uups:", t) }
            .doOnCancel { -> log.info { "cancel" } }

    val m = session.send(answerFlux).and(recFlux)
            .doAfterTerminate { ->
              log.info { "mono  afterTerminate" }
              removeProcessor.run()
            }
    return m

  }

  private fun handleUndoCmd(room: Room, undoParam: UndoParam) {
    log.info { "in room:${room.id}, undo:$undoParam " }
    room.game?.undo(undoParam)
  }
 

  private fun handleTeamCmd(room: Room, param: TeamParam) {
    log.info { "in room:${room.id}, teamParam: $param" }
    room.game?.let {
      // find the player in room
      val player = room.players.find { p -> p.id == param.playerId }
      if (player == null) {
        log.info { "player: ${param.playerId} is not in room:${room.id}" }
        return
      }
      // find team in game
      val team = it.teams.find { t -> t.id == param.teamId }
      if (team == null) {
        log.info { "team: ${param.teamId} is not in game" }
        return
      }
      when (param.operation) {
        PlayerGroupOperation.join -> team.addPlayer(player)
        PlayerGroupOperation.leave -> team.removePlayer(player)
      }

    }
  }

  private fun handleGameCreate(room: Room, param: GameCreateParam) {
    log.info { "in room:${room.id}, create new game:$param " }
    room.game = gameBuilder.newGame(param.nrOfTeams)
  }

  private fun handleHitCmd(room: Room, hit: HitParam) {
    log.info { "in room:${room.id}, got hit:$hit " }
    room.game?.addHit(Hit(hit.playerId, hit.targetId, hit.modifier, hit.usageId))
  }

  private fun handleFinishPlayCmd(room: Room, finishParam: FinishParam) {
    log.info { "in room:${room.id}, finishPlay:$finishParam " }
    room.game?.finishPlay(finishParam)
  }

  private fun handleRoomCmd(room: Room, req: RoomParam) {
    log.info { "in room:${room.id}, handle: $req " }
    val player = findPlayer(req.playerId)
    when (req.operation) {
      PlayerGroupOperation.join -> room.add(player)
      PlayerGroupOperation.leave -> room.remove(player)
    }
  }
  private fun handleKeepalive(req: KeepAliveParam) {
    log.info { "Keepalive ${req.dummy}" }
  }
  private fun findPlayer(playerId: String): Player {
    val res = dummyPlayers.find { r -> r.id == playerId }
    if (res == null) throw IllegalArgumentException("Player with id not found:$playerId")
    return res
  }

  fun roomPredicate(): (URI) -> Boolean {
    val pp: (URI) -> Boolean = { u: URI ->
      val uriComp = UriComponentsBuilder.fromUri(u).build()
      val roomId = uriComp.pathSegments.last()
      log.trace { "testing existence of room:${roomId}" }
      existsRoom(roomId)
    }
    return pp
  }

  private fun existsRoom(roomId: String): Boolean =
          dummyRooms.find { r -> r.id == roomId }.let { r -> r != null }

  private fun findRoom(roomId: String): Room {
    val res = dummyRooms.find { r -> r.id == roomId }
    if (res == null) throw IllegalArgumentException("Room with id not found:$roomId")
    return res
  }

 
}