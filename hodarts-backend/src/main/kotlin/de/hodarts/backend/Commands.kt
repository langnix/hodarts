package de.hodarts.backend

import kotlin.reflect.KClass

// room...
enum class PlayerGroupOperation {
  join,
  leave
}

interface PlayerIdBased {
  val playerId: String
}

// join/leave the room
data class KeepAliveParam(val dummy: Int)
data class RoomParam(val operation: PlayerGroupOperation, override val playerId: String) : PlayerIdBased
data class GameCreateParam(val nrOfTeams: Int)
data class TeamParam(val operation: PlayerGroupOperation, val teamId: String, override val playerId: String) : PlayerIdBased

// hit & undo
data class HitParam(
    /** die Id des players, der diesen Hit geworfen hat */
    override val playerId: String,
    /** das Feld, das getroffen wurde */
    val targetId: String,
    /** wie das Feld getroffen wurde */
    val modifier: ModifierEnum = ModifierEnum.single,
    /** das Feld/Wert, auf das der Hit zaehlen soll */
    val usageId: String = targetId
) : PlayerIdBased

data class FinishParam(override val playerId: String) : PlayerIdBased
data class UndoParam(
    /** wie viele hit's zurueckgenommen werden sollen */
    val count: Int = 1)

// game
enum class CommandEnum(
    // * --> star-projection
    val kt: KClass<*>) {
  room(RoomParam::class),
  gameCreate(GameCreateParam::class),
  team(TeamParam::class),

  hit(HitParam::class),
  finishPlay(FinishParam::class),
  undo(UndoParam::class),
  keepAlive(KeepAliveParam::class)
}


data class Command(val cmd: CommandEnum, val param: Any)


