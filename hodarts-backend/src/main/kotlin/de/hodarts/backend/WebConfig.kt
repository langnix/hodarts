package de.hodarts.backend

import mu.KotlinLogging
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.server.PathContainer
import org.springframework.web.reactive.HandlerMapping
import org.springframework.web.reactive.function.server.router
import org.springframework.web.reactive.handler.SimpleUrlHandlerMapping
import org.springframework.web.reactive.socket.server.support.WebSocketHandlerAdapter
import org.springframework.web.server.ServerWebExchange
import java.net.URI


private val log = KotlinLogging.logger {}

@Configuration
class WebSocketConfig(
    val roomHandler: RoomWebsocketHandler
) {
  /** damit der WS-Handshake funktioniert */
  @Bean
  fun websocketHandlerAdapter() = WebSocketHandlerAdapter()

  class MySpecialSimpleUrlHandlerMapping : SimpleUrlHandlerMapping() {
    override fun lookupHandler(lookupPath: PathContainer, exchange: ServerWebExchange): Any? {
      log.trace { "looking for :${exchange.request.uri}" }
      val match = super.lookupHandler(lookupPath, exchange)
      if (match is Pair<*, *>) {
        @Suppress("UNCHECKED_CAST")
        val pred: (URI) -> Boolean = match.second as (URI) -> Boolean
        if (!pred.invoke(exchange.request.uri)) return null
        return match.first
      }
      return match
    }
  }

  @Bean
  fun handlerMapping(): HandlerMapping {
    val handlerMapping = MySpecialSimpleUrlHandlerMapping()

    handlerMapping.urlMap = mapOf(
        "/ws/room/*" to Pair(roomHandler, roomHandler.roomPredicate())
    )
    handlerMapping.order = 1

    return handlerMapping
  }

//  @Bean
//  fun indexRouter(
//      @Value("classpath:/public/index.html") html: Resource) =
//      router {
//        GET("/") {  ok().contentType(MediaType.TEXT_HTML).bodyValue(html) }
//      }
//

  @Bean
  fun indexRedirect() =
      router {
        GET("/") {
          temporaryRedirect(URI.create("/index.html")).build()
        }

      }


}