package de.hodarts.backend


interface MinimalRepository<T : IdBased> {
    fun findAll(): List<T>
    fun delete(id: String): T?
    fun save(entity: T): T
    fun getById(id: String): T
}

interface GameRepository : MinimalRepository<Game>
interface RoomRepository : MinimalRepository<Room>
