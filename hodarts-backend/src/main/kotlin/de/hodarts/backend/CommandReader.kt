package de.hodarts.backend

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.stereotype.Component
import java.io.InputStream

@Component
class CommandReader(val objectMapper: ObjectMapper) {
  fun fromInputStream(ins: InputStream): Command {
    val tree = objectMapper.readTree(ins)
    if (!tree.isObject) throw IllegalArgumentException("non object ")
    if (!tree.has("cmd")) throw IllegalArgumentException("missing type field")
    val typeNode = tree.get("cmd")
    val type = CommandEnum.valueOf(typeNode.asText())
    val javaClass = type.kt.java
    val cmd = objectMapper.treeToValue(tree["param"], javaClass)
    return Command(type, cmd)

  }
}