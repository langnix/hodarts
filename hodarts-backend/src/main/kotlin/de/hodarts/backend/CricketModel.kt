package de.hodarts.backend

import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonProperty
import mu.KotlinLogging
import java.lang.Integer.min
import java.time.Instant

private val log = KotlinLogging.logger {}


interface IdBased {
  val id: String
}


data class Player(
    override val id: String,
    var name: String,
    var abbrevation: String
) : IdBased

data class Room(
    override val id: String) : IdBased {
  val players = mutableSetOf<Player>()
  var game: Game? = null

  /** alle spieler des raumes, die gerade nicht in einem team des aktuellen spiels sind */
  @JsonProperty
  fun availablePlayers(): Set<Player> {
    val res = players.toMutableSet()
    val safeGame = game // da game ein var ist, muss
    safeGame?.let {
      it.teams.forEach { t -> res.removeAll(t.players) }
    }
    return res
  }

  fun add(player: Player) {
    players.add(player)
  }

  fun remove(player: Player) {
    players.remove(player)
    game?.let {
      it.teams.forEach({ tt -> tt.removePlayer(player) })
    }
  }

}


data class Target(
    val id: String,
    val label: String,
    val value: Int? = null,
    val bullsEye: Boolean = false,
    val noExtra: Boolean = false,
    val noSingleClick: Boolean = false)

data class TargetStat(val targetId: String, var count: Int = 0)

data class AccountedHit(val target: Target, val modifier: ModifierEnum, val usageId: String, val count: Int)
data class Team(
    @JsonIgnore
    val game: Game,

    val id: String,
    @JsonIgnore
    val statsByTargetId: Map<String, TargetStat>,
    var score: Int = 0,
    /**    Fahrkarten **/
    var ticket: Int = 0
) {

  @JsonProperty
  fun activeHits(): List<AccountedHit> {
    return game.activeHitsOfTeam(this).map { h -> AccountedHit(game.targetById(h.targetId), h.modifier, h.usageId, h.count()) }
  }

  val players = mutableListOf<Player>()

  @JsonProperty
  fun stats(): Map<String, Int> {
    return statsByTargetId.map { ee -> ee.key to ee.value.count }.toMap()
  }

  @JsonProperty
  fun nameShort(): String {
    if (players.isEmpty()) return id
    return players.map { p -> p.abbrevation }.joinToString("-")
  }

  @JsonProperty("complete")
  fun isComplete(): Boolean = statsByTargetId.values.find { ee -> ee.targetId != "miss" && ee.count < 3 } == null

  @JsonProperty("winner")
  fun isWinner(): Boolean {
    if (!this.isComplete()) return false
    val otherTeamLowerThanMe = game.teams
        .filter { t -> t != this }
        .firstOrNull { t -> t.score < score }
    return otherTeamLowerThanMe == null
  }

  /** add or move the player at the end */
  fun addPlayer(player: Player): Team {
    removePlayer(player)
    players.add(player)
    return this
  }

  fun removePlayer(player: Player): Team {
    players.removeIf { p -> p.id == player.id }
    return this
  }


  /** Praedikat, welches entscheidet, ob ein target "voll" ist */
  fun isTargetComplete(usageId: String) = countOf(usageId) >= 3
  fun countOf(targetId: String): Int = statsByTargetId[targetId]?.count ?: 0


  fun useHitOnTarget(hit: Hit, markAsTicket: Boolean = false): Int {
    val usageStat = statsByTargetId[hit.usageId]
    if (usageStat == null) return hit.count()

    if (hit.usageId == "miss") {
      // infinite Platz ...
      usageStat.count += hit.count()
      // Fahrkarte ???
      if (markAsTicket) ticket++
      hit.modifications.add(Modification(this, hit.count(), 0, markAsTicket))

      return 0
    }

    val hitCount = hit.count()
    val useCount = min(3 - usageStat.count, hitCount)

    usageStat.count += useCount
    hit.modifications.add(Modification(this, useCount))
    return hitCount - useCount
  }

  fun acceptHit(hit: Hit, remainingHit: Int): Int {
    val usageStat = statsByTargetId[hit.usageId]!!
    if (usageStat.count >= 3) return score
    // jetzt wird's teuer ...
    val scorePoints = hit.score() * remainingHit
    score += scorePoints
    hit.modifications.add(Modification(this, 0, scorePoints))
    return score
  }

  fun undoModification(usageId: String, mm: Modification) {
    val usageStat = statsByTargetId[usageId]!!
    usageStat.count -= mm.usageCnt
    score -= mm.scorePoints
    if (mm.ticketInc) ticket--
  }

  fun hasPlayer(playerId: String): Boolean {
    return players.find { p -> p.id == playerId } != null
  }

  fun activePlayerInRound(round: Int): Player? {
    if (players.isEmpty()) return null
    val playerNr = round % players.size
    return players[playerNr]
  }


}

data class Modification(val team: Team, val usageCnt: Int, val scorePoints: Int = 0, val ticketInc: Boolean = false)

enum class ModifierEnum(val count: Int) {
  single(1),
  double(2),
  triple(3)
}


data class Hit(
    /** die Id des players, der diesen Hit geworfen hat */
    val playerId: String,
    /** das Feld, das getroffen wurde */
    val targetId: String,
    /** wie das Feld getroffen wurde */
    val modifier: ModifierEnum = ModifierEnum.single,
    /** das Feld/Wert, auf das der Hit zaehlen soll */
    val usageId: String = targetId
) {

  val modifications = mutableListOf<Modification>()

  // Wie oft kann der Wert auf die usage gezahlt werden ?
  fun count(): Int {
    return when (usageId) {
      "double" -> 1
      "triple" -> 1
      else -> modifier.count
    }
  }

  // Was ist der Wert wenn 1x gezaehlt?
  fun score(): Int {
    val baseScore =
        when (targetId) {
          "miss" -> 0
          "bull" -> 25
          // nur noch gueltige Zahlen ...hoffen wir mal :-)
          else -> targetId.toInt()
        }
    val factor = when (usageId) {
      "double" -> modifier.count
      "triple" -> modifier.count
      else -> 1
    }
    return baseScore * factor
  }
}

data class Game(
    override val id: String,
    val targets: List<Target>,
    var lastUpdate: Instant = Instant.now()) : IdBased {
  @JsonProperty
  val teams: MutableList<Team> = mutableListOf()
  val hits: MutableList<Hit> = mutableListOf()

  @JsonProperty
  fun activeTeamNr(): Int = (hits.size / 3) % teams.size

  //@JsonProperty
  fun activeTeam(): Team = teams[activeTeamNr()]

  @JsonProperty
  fun activeRound() = hits.size / (teams.size * 3)

  @JsonProperty
  fun activePlayer(): Player? = activeTeam().activePlayerInRound(activeRound())

  @JsonProperty("complete")
  fun isComplete(): Boolean = teams.firstOrNull { ee -> ee.isWinner() } != null

  fun targetById(targetId: String): Target {
    return targets.find { t -> t.id == targetId }!!
  }

  fun addHit(ev: Hit, markAsTicket: Boolean = false) {
    log.debug { "processing: $ev" }

    val playTeam = this.activeTeam()
    if (!playTeam.hasPlayer(ev.playerId)) {
      return
    }
    val remainingHit = playTeam.useHitOnTarget(ev, markAsTicket)

    if (remainingHit > 0) {
      teams.filter { t -> t != playTeam } // alle anderen
          .forEach { t -> t.acceptHit(ev, remainingHit) }
    }
    hits.add(ev)
  }

  fun finishPlay(finishParam: FinishParam) {
    val remaingHitsInThisPlay = 3 - (hits.size % 3)
    for (i in 1..remaingHitsInThisPlay)
      addHit(Hit(finishParam.playerId, "miss", ModifierEnum.single), i == 3)

  }

  fun undo(undoParam: UndoParam) {
    for (n in 1..undoParam.count) {
      val matchingHit = hits.lastOrNull()
      if (matchingHit != null) {
        val hh = hits.removeAt(hits.lastIndex)
        hh.modifications.forEach { mm -> mm.team.undoModification(hh.usageId, mm) }
      }
    }
  }

  fun activeHitsOfTeam(team: Team): List<Hit> {
    val itsMe = (activeTeam() == team)

    if (itsMe) {
      val len = hits.size % 3
      val startAt = hits.size - len
      return hits.subList(startAt, startAt + len)
    } else {
      val hitsPerRound = teams.size * 3
      val curRoundNr = (hits.size / hitsPerRound)
      val teamNr = teams.indexOf(team)
      val curTeamNr = activeTeamNr()

      val useRound = if (teamNr < curTeamNr) curRoundNr else curRoundNr - 1
      val startAt = useRound * hitsPerRound + teamNr * 3

      return if (startAt >= 0) hits.subList(startAt, startAt + 3) else emptyList()

    }
  }


}

