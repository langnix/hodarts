package de.hodarts.backend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HodartsBackendApplication

fun main(args: Array<String>) {
	runApplication<HodartsBackendApplication>(*args)
}
