package de.hodarts.backend

val dummyPlayers = listOf(
        Player("p123", "Maggus", "Me"),
        Player("p124", "Johanna", "Js"),
        Player("p876", "Arne", "A"),
        Player("p99", "Daniel", "D"),
        Player("p432", "Marcin", "Mw"),
        Player("p987", "Jojo", "JJ"),
        Player("p1", "TJ", "TJ"))

val dummyRooms = listOf(Room("Test"), Room("Sonntags"))
