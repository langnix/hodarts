package de.hodarts.backend

import org.springframework.stereotype.Component
import kotlin.random.Random

@Component
class GameLogic {
  fun newGame(nrOfTeams: Int): Game {
    val targets = genTargets()

    val res = Game(id = Random.nextInt(1, 5000).toString(), targets = targets)
    val teams = genTeams(res, nrOfTeams, targets)
    res.teams.addAll(teams)
    return res
  }

  fun genTargets(): List<Target> {
    val res = ArrayList<Target>(3 + 9)
    for (nr in 20 downTo 12)
      res.add(Target(id = nr.toString(), label = nr.toString(), value = nr))

    res.add(Target(id = "double", label = "D", noExtra = true, noSingleClick = true))
    res.add(Target(id = "triple", label = "T", noExtra = true, noSingleClick = true))
    res.add(Target(id = "bull", label = "B", noExtra = true, bullsEye = true))
    res.add(Target(id = "miss", label = "-", noExtra = true))

    return res
  }


  fun genTeams(game: Game, nrOfTeams: Int, targets: List<Target>): List<Team> {
    val teams = (1..nrOfTeams).map {
      genTeam(game, it, targets)
    }
    return teams
  }

  fun genTeam(game: Game, idx: Int, targets: List<Target>): Team {
    val stats = targets.map { it.id to TargetStat(it.id, 0) }.toMap()
    val team = Team(id = idx.toString(), statsByTargetId = stats, game = game)
    return team
  }

}
