package de.hodarts.backend

import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux

@CrossOrigin
@RestController
class MainApi {

//    @PostMapping("/api/games")
//    fun Mono<GameCreated> startGame

    @GetMapping("/api/players")
    fun listAllPlayers(): Flux<Player> {
        return Flux.fromIterable(dummyPlayers)
    }


    @GetMapping("/api/rooms")
    fun listAllRooms(): Flux<Room> {
        return Flux.fromIterable(dummyRooms)
    }
}
