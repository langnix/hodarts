package de.hodarts.backend

import org.springframework.stereotype.Component

open class MinimalRepositoryInMem<T : IdBased> : MinimalRepository<T> {
    val entities: MutableMap<String, T> = hashMapOf()
    override fun findAll(): List<T> = entities.values.toList()
    override fun delete(id: String): T? = entities.remove(id)
    override fun save(entity: T): T {
        entities.put(entity.id, entity)
        return entity
    }

    override fun getById(id: String): T = entities.getValue(id)
}

@Component
class GameRepositoryInMem : MinimalRepositoryInMem<Game>(), GameRepository


@Component
class RoomRepositoryInMem : MinimalRepositoryInMem<Room>(), RoomRepository