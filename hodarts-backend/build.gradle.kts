import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


plugins {
	id("org.springframework.boot") version "2.2.6.RELEASE"
	id("io.spring.dependency-management") version "1.0.9.RELEASE"
	kotlin("jvm") version "1.3.71"
	kotlin("plugin.spring") version "1.3.71"

	// Visualisierung der task-Abhaengigkeiten, eg: gradlew :build :taskTree
	id("com.dorongold.task-tree") version "1.5"
}

apply(plugin = "io.spring.dependency-management")

//Variante: nur das dependency-management von spring-boot  nutzen
// siehe: https://docs.spring.io/spring-boot/docs/current/gradle-plugin/reference/html/#managing-dependencies-using-in-isolation
//the<io.spring.gradle.dependencymanagement.dsl.DependencyManagementExtension>().apply {
//	imports {
//		mavenBom("org.springframework.boot:spring-boot-dependencies:2.2.6.RELEASE")
//	}
//}

tasks.getByName<Jar>("jar") {
	// bei enabled=true -> es wird nur das "normale" jar gebaut, der task bootJar tut nichts
	enabled = true
}

//siehe https://stackoverflow.com/questions/26697999/gradle-equivalent-to-mavens-copy-dependencies
task("copyDependencies", Copy::class) {
	from(configurations.runtimeClasspath).into("$buildDir/dependencies")
}

group = "de.hodarts"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-webflux")
	implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")

	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

	implementation("io.github.microutils:kotlin-logging:1.7.9")

	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
	testImplementation("io.projectreactor:reactor-test")
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}
